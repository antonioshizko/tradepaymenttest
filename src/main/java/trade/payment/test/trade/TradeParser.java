package trade.payment.test.trade;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class TradeParser {

    /**
     * Parse the input message, extract i,p,t values to corresponding trade.payment.test.trade.Trade values.
     *
     * @param line message for Stock exchange as a string with ; separated values
     *
     * @return new trade.payment.test.trade.Trade if message input is correct (one i,p,t values passed ) or throw exception
     *
     * @throws Exception Invalid message input for messages with duplictes or  missing elements
     */
    public static Trade parse(String line) throws Exception {
        Trade trade = new Trade();
        if(line==null && line.isEmpty()){
            throw new Exception("empty or invalid input message");
        }
        List<String> listsik = Arrays.stream(line.split(";")).collect(Collectors.toList());

        if(listsik.stream().filter(u ->u.substring(0,1).equals("i")
                  || u.substring(0,1).equals("t")
                  ||u.substring(0,1).equals("p"))
                  .collect(Collectors.toList()).size()!=3){
             throw  new Exception("Invalid message input");
        }else{
              listsik.stream().map(
                      p -> {
                          if(p.substring(0,1).equals("i")){
                              trade.setIsin(p.substring(1));
                          }else if(p.substring(0,1).equals("p")){
                              trade.setPrice(new BigDecimal(p.substring(1)));
                          }else if(p.substring(0,1).equals("t")){
                              trade.setType(Integer.valueOf(p.substring(1)));
                          }
                          return trade;
                      }
              ).collect(Collectors.toList());
          }
        return trade;
    }
}