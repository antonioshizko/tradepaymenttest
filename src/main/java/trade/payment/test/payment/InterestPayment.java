package trade.payment.test.payment;

public class InterestPayment {

    private static int PAYMENT_DAY_TOLERANCE = 5;
    private Payment currentPayment;

    public InterestPayment(Payment currentPayment) {
        this.currentPayment = currentPayment;
    }
    /**
     * Update the payment if the new payment is within 5 days of the current payment.
     *
     * @param new_payment
     * The new payment
     * @return True if the payment was updated, false otherwise.
     */
    public boolean updatePayment(Payment new_payment) {
        synchronized (this){
            if (Math.abs(new_payment.getDayNumber() - currentPayment.getDayNumber()) <= PAYMENT_DAY_TOLERANCE) {
                this.currentPayment = new_payment;
                return true;
            }
            return false;
        }
    }

    public Payment getCurrentPayment() {
        return currentPayment;
    }
}
