import org.junit.Before;
import org.junit.Test;
import trade.payment.test.payment.InterestPayment;
import trade.payment.test.payment.Payment;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;


public class TestPAyment {

    Payment currentPayment;
    Payment newPayment;
    InterestPayment currentInterestPayment;

    @Before
    public void setUp() {
         currentPayment = new Payment(20L, new BigDecimal(26.0));
         newPayment = new Payment(12L, new BigDecimal(56.0));
         currentInterestPayment = new InterestPayment(currentPayment);
    }

    @Test
    public void testDatesNotWithinARangeBeforeTheCurrentDate(){
        assertEquals("Dates ARE   within a range of 5 days ",false, currentInterestPayment.updatePayment(newPayment));
        assertEquals(currentInterestPayment.getCurrentPayment(), currentPayment);

    }

    @Test
    public void testDatesWithinARangeBeforeTheCurrentDate(){
        newPayment.setDayNumber(17L);
        assertEquals("Dates are NOT within a range of 5 days ",true, currentInterestPayment.updatePayment(newPayment));
        assertEquals(currentInterestPayment.getCurrentPayment(), newPayment);
    }

    @Test
    public void testDatesNotWithinARangeAfterTheCurrentDAte(){
        assertEquals("Dates ARE within a range of 5 days ",false, currentInterestPayment.updatePayment(newPayment));
        assertEquals(currentInterestPayment.getCurrentPayment(), currentPayment);
    }

    @Test
    public void testDatesWithinARangeAfterTheCurrentDate(){
        newPayment.setDayNumber(17L);
        assertEquals("Dates are  NOT within a range of 5 days ",true, currentInterestPayment.updatePayment(newPayment));
        assertEquals(currentInterestPayment.getCurrentPayment(), newPayment);
    }

    @Test(expected = NullPointerException.class)
    public void testNPE(){
        newPayment = null;
        assertEquals("Dates are  NOT within a range of 5 days ",true, currentInterestPayment.updatePayment(newPayment));
        assertEquals(currentInterestPayment.getCurrentPayment(), newPayment);
    }
}
